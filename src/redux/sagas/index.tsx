import { fork, all } from 'redux-saga/effects';
import headerSagas from '../../app/containers/header/headerSaga';

function* subSagas() {
    yield all([
        yield fork(headerSagas),
    ])
}
// We fork the subsagas separately incase we want to fork a login-saga and only
// initiate the subsagas if we are authenticated ..
// Basically to give us more configuration options
export default function* root() {
    yield fork(subSagas)
}
