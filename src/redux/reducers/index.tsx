import { combineReducers } from 'redux'
import headerReducer from '../../app/containers/header/headerReducer';

const reducer = combineReducers({
  headerReducer,
})

export default reducer
