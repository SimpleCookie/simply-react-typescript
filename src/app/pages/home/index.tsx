import React from 'react';
import { connect } from 'react-redux';

class Home extends React.Component<{}, {}> {
    public render() {
        return (
            <div>
                <h2>Borta bra men hemma bäst!</h2>
            </div>
        );
    }
}

export default connect()(Home);
