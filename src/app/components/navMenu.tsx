import React from 'react';
import { MenuOption } from '../containers/header/headerInterfaces';

interface Props {
    options: MenuOption[];
}

class NavMenu extends React.Component<Props, {}> {
    public render() {
        const { options } = this.props;
        return <ul>{options.map(opt => this.renderOption(opt))}</ul>;
    }

    private renderOption = option => {
        return (
            <li key={option.id}>
                <a href={option.object_slug}>{option.title}</a>
            </li>
        );
    };
}

export default NavMenu;
