import * as React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk';

import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'

import reducers from './../redux/reducers';
import rootSaga from './../redux/sagas';
import Routes from './routes'
import Footer from './containers/footer/';
import Header from './containers/header/';

// create the saga middleware
const sagaMiddleware = createSagaMiddleware()
// mount it on the Store
const store = createStore(
  reducers,
  {},
  compose(applyMiddleware(thunkMiddleware, sagaMiddleware))
)

// then run the saga
sagaMiddleware.run(rootSaga)

const App = (
    <Provider store={store}>
        <Router>
            <div>
                <Header />
                <main className="app-content">
                    <div className="container">
                        <Routes />
                    </div>
                </main>
                <Footer />
            </div>
        </Router>
    </Provider>
)

export default App;
