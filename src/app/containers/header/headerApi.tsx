import axios from 'axios';
import constants from '../../../lib/constants/globals';

export default {
    fetchMenu() {
        return axios
            .get(constants.getMenuUrl)
            .then(result => {
                return result.data;
            })
            .catch(error => {
                console.log(error);
                return error;
            });
    }
};
