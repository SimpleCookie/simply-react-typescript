import { takeEvery, call, all, put, fork } from 'redux-saga/effects';
import headerActions from './headerActions';
import headerApi from './headerApi';

function* getMenuSaga() {
    try {
        const fetchedMenu = yield call(headerApi.fetchMenu);
        yield put(headerActions.didGetMenu(fetchedMenu.items));
    } catch (error) {
        console.error('Something went wrong', error);
    }
}

function* willGetMenuWorker() {
    try {
        yield takeEvery(headerActions.willGetMenu().type, getMenuSaga);
    } catch (error) {
        console.error('Something went wrong', error);
    }
}

export default function* headerSagas() {
    yield all([fork(willGetMenuWorker)]);
}
