import React from 'react';
import { connect } from 'react-redux';
import { MenuOption } from './headerInterfaces';
import headerActions from './headerActions';
import NavMenu from '../../components/navMenu';

interface Props {
    dispatcher: {
        getHeaderMenu: () => void;
    };
    // Reducers
    headerReducer: {
        menu: MenuOption[];
    };
}

const mapStateToProps = state => ({
    headerReducer: state.headerReducer,
});

const mapDispatchToProps = dispatch => ({
    dispatcher: {
        getHeaderMenu: () => {
            dispatch(headerActions.willGetMenu());
        },
    },
});

class Header extends React.Component<Props, {}> {
    public static getDerivedStateFromProps(props, state) {
        console.log('New props', props)
        return state
    }

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public componentDidMount() {
        this.props.dispatcher.getHeaderMenu();
    }

    public render() {
        const { menu } = this.props.headerReducer
        return (
            <div>
                <NavMenu options={menu} />
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);
