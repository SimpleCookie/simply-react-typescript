import React from 'react';
import { connect } from 'react-redux';

interface Props {
    // Reducers
    authReducer: {
        token: string;
        authority: number;
        isAuthenticating: boolean;
    };
}

const mapStateToProps = state => ({
    authReducer: state.authReducer,
});

class Footer extends React.Component<Props, {}> {
    public render() {
        return (
            <div>
                <h2>Footer ...</h2>
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    {}
)(Footer);
