import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Switch, Route } from 'react-router-dom';
import * as Pages from './pages';

// Define your props, including dispatches
interface Props {
    // Dispatches
    dispatcher: any;
}

const mapStateToProps = state => ({
    authReducer: state.authReducer
});

class Routes extends React.Component<Props, {}> {
    public render() {
        return (
            <Switch>
                <Route name="Home" component={Pages.Home} exact={true} path="/" />
            </Switch>
        );
    }
}

export default withRouter(connect(
    mapStateToProps,
    {}
)(Routes));
