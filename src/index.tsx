// import * as React from 'react';
import { render } from 'react-dom';
import './assets/styles/style.scss';
import App from './app/app';

// render the application

const rootEl = document.getElementById('root');

render(App, rootEl);
