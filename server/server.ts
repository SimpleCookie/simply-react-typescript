var express = require('express');
var cors = require('cors');

// Root resolver
var root = {
    message: () => 'Hello World!'
};
// Create an express server
var app = express();
app.use('/api', cors());
app.listen(3001, () => console.log('Server Now Running On localhost:3001/api'));
