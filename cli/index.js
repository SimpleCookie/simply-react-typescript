const helpers = require('./helper');
const commander = require('commander');
const inquirer = require('inquirer');
const {
    requestName,
    getNameChoices,
    verifyName,
    add,
} = helpers

console.log(inquirer.prompt)

const doPromptVerifyName = (name, capitalizedName, lowercasedName) => {
    inquirer.prompt(verifyName(name, capitalizedName)).then(
        shouldUseSuggestedName => {
            if (shouldUseSuggestedName.bool) {
                add(capitalizedName, lowercasedName);
            } else {
                add(name, lowercasedName);
            }
        }
    );
}

const doPromptName = () => {
    inquirer.prompt(requestName).then(nameAnswers => {
        const names = getNameChoices(nameAnswers.name);
        const { name, capitalizedName, lowercasedName } = names;

        if (name === capitalizedName) {
            add(name, lowercasedName);
        } else {
            doPromptVerifyName(name, capitalizedName, lowercasedName)
        }
    });
};

commander
    .version('0.0.1')
    .description('Reactboilerplate CLI tool by Marcus Ekström');

commander
    .command('add')
    .alias('a')
    .description('Add a new container component with corresponding utilities')
    .action(doPromptName);

commander.parse(process.argv);
