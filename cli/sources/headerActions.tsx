const headerConstants = {
    willGetMenu: 'WILL_GET_MENU',
    didGetMenu: 'DID_GET_MENU'
};

export default {
    willGetMenu: (payload?: any) => ({
        type: headerConstants.willGetMenu
    }),

    didGetMenu: (payload?: any) => ({
        type: headerConstants.didGetMenu,
        menu: payload
    })
};
