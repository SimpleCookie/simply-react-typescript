export interface MenuOption {
    id: number;
    parent: number;
    order: number;
    title: string;
    url: string;
    object: string;
    object_id: number;
    object_slug: string;
    children?: MenuOption[];
}
