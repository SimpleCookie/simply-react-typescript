import actions from './headerActions';

const initialState = {
    menu: [],
};

const handleWillGetMenu = (state, action) => ({
    ...state,
    isFetchingMenu: true,
});

const handleDidGetMenu = (state, action) => ({
    ...state,
    isFetchingMenu: false,
    menu: action.menu,
});

const handlers = {
    [actions.didGetMenu().type]: handleDidGetMenu,
    [actions.willGetMenu().type]: handleWillGetMenu,
};

const reducer = (state = initialState, action) =>
    handlers[action.type] ? handlers[action.type](state, action) : state;

export default reducer;
