const fs = require('fs');

module.exports = {
    add: (capitalizedName, lowercasedName) => {
        const stream = fs.createWriteStream(`cli/bin/${capitalizedName}.tsx`);
        stream.once('open', () => {
            stream.write(`My first ${lowercasedName} row\n`);
            stream.write(`My second ${capitalizedName} row\n`);
            stream.end();
        });
    },
    getNameChoices: name => {
        const lowercasedName = name.toLowerCase();
        const firstUppercase = lowercasedName.charAt(0).toUpperCase();
        return {
            name,
            capitalizedName: firstUppercase + lowercasedName.substr(1),
            lowercasedName,
        };
    },
    verifyName: (name, suggestedName) => {
        return [
            {
                type: 'confirm',
                name: 'bool',
                message: `Do you want to name it ${suggestedName} instead of ${name}?\n`,
            },
            ,
        ];
    },
    requestName: [
        {
            type: 'input',
            name: 'name',
            message: 'Enter name ...\n',
        },
    ],
};
